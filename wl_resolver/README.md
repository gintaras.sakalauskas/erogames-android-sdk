# Whitelabel Resolver Script

...

### Requirements
* [apktool](https://ibotpeaches.github.io/Apktool/)
* [xmlstarlet](https://formulae.brew.sh/formula/xmlstarlet)
* [Android build tools](https://developer.android.com/studio/releases/build-tools)

### How to use
* Install [the framework](framework-res.apk) (it only needs to be done once):
```
apktool if path/to/framework-res.apk
```
* Open terminal and go to directory with APK (`cd path/to/apk_directory`).
* Put the config file "wl.properties" (with appropriate changes) into that directory.
* Run script with `whitelabel` name as argument:
`bash path/to/wl_resolver.sh --whitelabel xvideos`

Optional arguments (for Erogames Analytics): `--anltcs_source_id`, `--anltcs_wl_id`, `--anltcs_wl_access_key`
