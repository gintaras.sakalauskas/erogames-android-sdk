package com.erogames.api.model

import androidx.annotation.Keep
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Keep
@Serializable
data class QuestData(
    val quest: Quest,
    val leaders: List<Leader>,
    @SerialName("best_players")
    val bestPlayers: List<BestPlayer>,
    @SerialName("user_clan_attempt")
    val userClanAttempt: UserClanAttempt?,
    @SerialName("user_attempt")
    val userAttempt: UserAttempt?,
    @SerialName("user_score")
    val userScore: UserScore,
)

@Keep
@Serializable
data class Quest(
    val id: Int,
    val title: String,
    val description: String?,
    @SerialName("cover_url")
    val coverUrl: String?,
    @SerialName("finished_at")
    val finishedAt: String,
)

@Keep
@Serializable
data class Leader(
    val position: Int,
    val name: String,
    @SerialName("cover_picture_url")
    val coverPictureUrl: String?,
    @SerialName("clan_leader_name")
    val clanLeaderName: String?,
    val score: Long,
)

@Keep
@Serializable
data class BestPlayer(
    val position: Int,
    val name: String,
    @SerialName("cover_picture_url")
    val coverPictureUrl: String?,
    val score: Long,
)

@Keep
@Serializable
data class UserClanAttempt(
    val position: Int,
    val name: String,
    @SerialName("cover_picture_url")
    val coverPictureUrl: String?,
    @SerialName("clan_leader_name")
    val clanLeaderName: String?,
    val score: Int,
)

@Keep
@Serializable
data class UserAttempt(
    val position: Int,
    val name: String,
    @SerialName("cover_picture_url")
    val coverPictureUrl: String?,
    val score: Int,
)

@Keep
@Serializable
data class UserScore(
    val total: Int,
    @SerialName("earned_points")
    val earnedPoints: List<EarnedPoint>,
)

@Keep
@Serializable
data class EarnedPoint(
    val points: Int,
    val title: String,
)