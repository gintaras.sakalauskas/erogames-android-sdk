package com.erogames.api.model

import androidx.annotation.Keep
import kotlinx.serialization.Serializable

@Keep
@Serializable
internal data class WhitelabelCache(
    val whitelabel: Whitelabel,
    val createdAt: Long
) {
    companion object {
        fun isWlCacheValid(whitelabelCache: WhitelabelCache): Boolean {
            val currentTimeStamp = System.currentTimeMillis() / 1000
            return (whitelabelCache.createdAt + (24 * 60 * 60)) > currentTimeStamp
        }
    }
}