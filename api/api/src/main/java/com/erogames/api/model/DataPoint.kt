package com.erogames.api.model

import androidx.annotation.Keep
import kotlinx.serialization.Serializable

@Keep
@Serializable
data class DataPoint(val field: String, val value: String)