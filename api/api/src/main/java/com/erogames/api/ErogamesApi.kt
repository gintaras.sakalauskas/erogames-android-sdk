package com.erogames.api

import android.content.Context
import android.util.Log
import androidx.annotation.Keep
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.erogames.api.model.*
import com.erogames.api.repository.Repository
import com.erogames.api.util.InjectorUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@Keep
class ErogamesApi private constructor(private val repository: Repository) {

    suspend fun loadUser(accessToken: String): User = repository.loadUser(accessToken)

    suspend fun proceedPayment(accessToken: String, paymentId: String?, amount: Int?) {
        repository.proceedPayment(accessToken, paymentId, amount)
    }

    @Deprecated("Will be removed.")
    suspend fun addDataPoints(accessToken: String, dataPoints: List<DataPoint>) {
        repository.addDataPoints(accessToken, dataPoints)
    }

    suspend fun loadCurrentQuest(accessToken: String): QuestData =
        repository.getCurrentQuestData(accessToken)

    suspend fun loadPaymentInfo(accessToken: String, paymentId: String): PaymentInfo =
        repository.getPaymentInfo(accessToken, paymentId)

    companion object {

        private const val TAG = "ErogamesApi"

        @Volatile
        private var instance: ErogamesApi? = null

        @JvmStatic
        fun getInstance(ctx: Context): ErogamesApi {
            return instance ?: synchronized(this) {
                return instance
                    ?: ErogamesApi(InjectorUtil.provideAuthRepository(ctx)).also { instance = it }
            }
        }

        @JvmStatic
        fun loadUser(ctx: Context, accessToken: String, listener: OnResult<User>) {
            getCoroutineScope(ctx).launch {
                var t: Throwable? = null
                val user: User? = withContext(Dispatchers.IO) {
                    try {
                        InjectorUtil.provideAuthRepository(ctx).loadUser(accessToken)
                    } catch (e: Exception) {
                        Log.e(TAG, "loadUser", e)
                        t = e
                        null
                    }
                }
                when {
                    user != null -> listener.onSuccess(user)
                    else -> listener.onFailure(t)
                }
            }
        }

        @JvmStatic
        fun proceedPayment(
            ctx: Context,
            accessToken: String,
            paymentId: String?,
            amount: Int?,
            listener: OnResult<Void?>
        ) {
            getCoroutineScope(ctx).launch {
                var t: Throwable? = null
                val baseStatusResp: BaseStatusResp? = withContext(Dispatchers.IO) {
                    val repo = InjectorUtil.provideAuthRepository(ctx)
                    try {
                        repo.proceedPayment(accessToken, paymentId, amount)
                    } catch (e: Exception) {
                        Log.e(TAG, "proceedPayment", e)
                        t = e
                        null
                    }
                }
                when {
                    baseStatusResp != null -> listener.onSuccess(null)
                    else -> listener.onFailure(t)
                }
            }
        }

        @JvmStatic
        @Deprecated("Will be removed in v2.0.0")
        fun addDataPoints(
            ctx: Context,
            accessToken: String,
            dataPoints: List<DataPoint>,
            listener: OnResult<Void?>,
        ) {
            getCoroutineScope(ctx).launch {
                var t: Throwable? = null
                val baseStatusResp: BaseStatusResp? = withContext(Dispatchers.IO) {
                    val repo = InjectorUtil.provideAuthRepository(ctx)
                    try {
                        repo.addDataPoints(accessToken, dataPoints)
                    } catch (e: Exception) {
                        Log.e(TAG, "addDataPointCollection", e)
                        t = e
                        null
                    }
                }
                when {
                    baseStatusResp != null -> listener.onSuccess(null)
                    else -> listener.onFailure(t)
                }
            }
        }

        @JvmStatic
        fun loadCurrentQuest(ctx: Context, accessToken: String, listener: OnResult<QuestData>) {
            getCoroutineScope(ctx).launch {
                var t: Throwable? = null
                val questData: QuestData? = withContext(Dispatchers.IO) {
                    val repo = InjectorUtil.provideAuthRepository(ctx)
                    try {
                        repo.getCurrentQuestData(accessToken)
                    } catch (e: Exception) {
                        Log.e(TAG, "loadCurrentQuest", e)
                        t = e
                        null
                    }
                }
                when {
                    questData != null -> listener.onSuccess(questData)
                    else -> listener.onFailure(t)
                }
            }
        }

        @JvmStatic
        fun loadPaymentInfo(
            ctx: Context,
            accessToken: String,
            paymentId: String,
            listener: OnResult<PaymentInfo>
        ) {
            getCoroutineScope(ctx).launch {
                var t: Throwable? = null
                val paymentInfo: PaymentInfo? = withContext(Dispatchers.IO) {
                    val repo = InjectorUtil.provideAuthRepository(ctx)
                    try {
                        repo.getPaymentInfo(accessToken, paymentId)
                    } catch (e: Exception) {
                        Log.e(TAG, "loadPaymentInfo", e)
                        t = e
                        null
                    }
                }
                when {
                    paymentInfo != null -> listener.onSuccess(paymentInfo)
                    else -> listener.onFailure(t)
                }
            }
        }

        private fun getCoroutineScope(ctx: Context): CoroutineScope = when (ctx) {
            is AppCompatActivity -> ctx.lifecycleScope
            else -> CoroutineScope(Dispatchers.Main)
        }
    }
}

interface OnResult<T> {
    fun onSuccess(data: T)
    fun onFailure(t: Throwable?)
}