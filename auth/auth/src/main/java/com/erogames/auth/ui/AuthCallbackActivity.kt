package com.erogames.auth.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.erogames.auth.ErogamesAuth
import com.erogames.auth.R
import com.erogames.auth.model.Result
import com.erogames.auth.model.Whitelabel
import com.erogames.auth.util.*


internal class AuthCallbackActivity : AppCompatActivity() {

    private var authCode: String? = null
    private var startAuth: Boolean = false
    private var targetAction: String? = ACTION_LOGIN
    private val viewModel by viewModels<AuthCallbackViewModel> {
        AuthCallbackViewModelFactory(
            InjectorUtil.provideAuthRepository(this),
            AuthUtil.getAccessKey(this),
            Pkce.getCodeVerifier(),
            AuthUtil.buildRedirectUri(this)
        )
    }
    private lateinit var actionTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth_callback)

        actionTextView = findViewById(R.id.auth_callback_action)

        viewModel.onAuth.observe(this, { onAuth(it) })
        viewModel.onLogout.observe(this, { onLogout(it) })
        viewModel.onWhitelabel.observe(this, { onWhitelabel(it) })

        targetAction = intent.action
        updateUI()
        when (intent.action) {
            ACTION_LOGIN, ACTION_SIGNUP -> {
                startAuth = true
                viewModel.loadWhitelabel()
            }
            ACTION_LOGOUT -> {
                startAuth = true
                viewModel.logout()
            }
            else -> handleDeepLinks(intent)
        }
    }

    /**
     * Observes authentication state.
     */
    private fun onAuth(result: Result<Unit>?) {
        when (result) {
            is Result.Loading -> Unit
            is Result.Success -> {
                val intent = Intent()
                intent.putExtra(ErogamesAuth.EXTRA_USER, ErogamesAuth.getUser(this))
                setResult(RESULT_OK, intent)
            }
            is Result.Error -> setErrorResult(result.error?.message.toString())
        }
        if (result !is Result.Loading) finish()
    }

    /**
     * Observes Whitelabel loading state. If whitelabel info has been loaded successfully then
     * appropriate authentication URL will generated (based on whitelabel's info) and launched.
     */
    private fun onWhitelabel(result: Result<Whitelabel>?) {
        when (result) {
            is Result.Loading -> Unit
            is Result.Success -> {
                Utils.launchUrl(this, viewModel.buildAuthUrl(intent.action == ACTION_SIGNUP))
            }
            is Result.Error -> {
                Log.e(TAG, "onWhitelabel", result.error)
                setErrorResult(result.error?.message.toString())
                if (result.error is ApiOutdatedException) {
                    ApiIssueDialog.show(this, true)
                } else {
                    finish()
                }
            }
        }
    }

    /**
     * Observes 'on logout' action.
     */
    private fun onLogout(result: Result<Boolean>?) {
        when (result) {
            is Result.Loading -> Unit
            is Result.Success -> {
                val webLogout = intent.getBooleanExtra(EXTRA_WEB_LOGOUT, false)
                if (webLogout.not()) {
                    viewModel.clearLocalData().also { finishResultOk() }
                    return
                }

                val whitelabel = viewModel.getWhitelabel()
                if (whitelabel == null) {
                    viewModel.clearLocalData().also { finishResultOk() }
                    return
                }

                val webLogoutUrl = viewModel.buildWebLogoutUrl()
                viewModel.clearLocalData()
                when (webLogoutUrl) {
                    null -> finishResultOk()
                    else -> Utils.launchUrl(this, webLogoutUrl)
                }
            }
            is Result.Error -> {
                setErrorResult(result.error?.message.toString())
                finish()
            }
        }
    }

    private fun finishResultOk() {
        setResult(RESULT_OK)
        finish()
    }

    private fun updateUI() {
        val actionRes = when (targetAction) {
            ACTION_LOGIN -> R.string.com_erogames_sdk_loggingin
            ACTION_SIGNUP -> R.string.com_erogames_sdk_signingup
            ACTION_LOGOUT -> R.string.com_erogames_sdk_signingout
            else -> R.string.com_erogames_sdk_empty_str
        }
        actionTextView.text = getLocalizedString(actionRes, viewModel.getLocale())
    }

    private fun setErrorResult(errMessage: String) {
        val intent = Intent()
        intent.putExtra(ErogamesAuth.EXTRA_ERROR, errMessage)
        setResult(RESULT_OK, intent)
    }

    override fun onResume() {
        super.onResume()
        if (!startAuth && TextUtils.isEmpty(authCode)) {
            finish()
        }
        startAuth = false
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        handleDeepLinks(intent)
    }

    private fun handleDeepLinks(intent: Intent) {
        intent.data?.let { uri ->
            Log.d(TAG, "Deep link: $uri")
            val isLogout = uri.getQueryParameter(LOGOUT_PARAM)
            if (isLogout != null) {
                setResult(RESULT_OK)
                finish()
                return
            }

            authCode = uri.getQueryParameter(CODE_PARAM)
            authCode?.let { viewModel.setCode(it) }
        }
    }

    companion object {
        fun signupAction(ctx: Activity) {
            val i = Intent(ctx, AuthCallbackActivity::class.java)
            i.action = ACTION_SIGNUP
            ctx.startActivityForResult(i, ErogamesAuth.REQUEST_CODE_AUTH)
        }

        fun loginAction(ctx: Activity) {
            val i = Intent(ctx, AuthCallbackActivity::class.java)
            i.action = ACTION_LOGIN
            ctx.startActivityForResult(i, ErogamesAuth.REQUEST_CODE_AUTH)
        }

        fun logoutAction(ctx: Activity, webLogout: Boolean) {
            val i = Intent(ctx, AuthCallbackActivity::class.java)
            i.action = ACTION_LOGOUT
            i.putExtra(EXTRA_WEB_LOGOUT, webLogout)
            ctx.startActivityForResult(i, ErogamesAuth.REQUEST_CODE_LOGOUT)
        }

        private const val TAG = "AuthCallbackActivity"
        private const val ACTION_SIGNUP = "${Constants.PACKAGE_NAME_PREFIX}.action.SIGNUP"
        private const val ACTION_LOGIN = "${Constants.PACKAGE_NAME_PREFIX}.action.LOGIN"
        private const val ACTION_LOGOUT = "${Constants.PACKAGE_NAME_PREFIX}.action.API_LOGOUT"

        private const val CODE_PARAM = "code"
        private const val LOGOUT_PARAM = "logout"
        private const val EXTRA_WEB_LOGOUT = "web_logout"
    }
}