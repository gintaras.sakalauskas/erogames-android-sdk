package com.erogames.auth.util

/**
 * This exception is thrown when a REST API is outdated.
 * @see com.erogames.auth.repository.Repository.loadWhitelabels()
 */
internal class ApiOutdatedException(message: String?) : Exception(message)