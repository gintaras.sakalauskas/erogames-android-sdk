package com.erogames.auth.util

import android.util.Base64
import kotlin.random.Random

/**
 * PKCE helper. See: [PKCE](https://www.oauth.com/oauth2-servers/pkce/)
 */
internal object Pkce {

    private val charPool: List<Char> =
        ('a'..'z') + ('A'..'Z') + ('0'..'9') + listOf('-', '.', '_', '~')
    private var codeVerifier: String? = null

    fun getCodeVerifier(generateNew: Boolean = false): String {
        if (codeVerifier == null || generateNew) {
            codeVerifier = generateCodeVerifier()
        }
        return codeVerifier!!
    }

    private fun generateCodeVerifier(): String {
        val strLength = (43..128).random()
        return (1..strLength)
            .map { Random.nextInt(0, charPool.size) }
            .map(charPool::get)
            .joinToString("")
    }

    fun generateCodeChallenge(codeVerifier: String): String = Base64.encodeToString(
        codeVerifier.sha256(),
        Base64.URL_SAFE or Base64.NO_PADDING or Base64.NO_WRAP
    )
}