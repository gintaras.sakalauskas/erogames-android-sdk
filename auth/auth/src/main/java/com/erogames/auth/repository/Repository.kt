package com.erogames.auth.repository

import android.content.Context
import com.erogames.auth.api.ApiService
import com.erogames.auth.model.*
import com.erogames.auth.util.ApiOutdatedException
import com.erogames.auth.util.Constants
import com.erogames.auth.util.InjectorUtil
import java.util.*

internal class Repository(private val apiService: ApiService, private val storage: Storage) {

    /**
     * Return JWT token from an API service.
     */
    suspend fun loadJwtToken(accessKey: String): String = apiService.getJwtToken(accessKey).token

    fun setWhitelabelId(whitelabelId: String) = storage.setWhitelabelId(whitelabelId)

    fun getWhitelabelId(): String = storage.getWhitelabelId()

    suspend fun loadWhitelabel(
        jwtToken: String,
        whitelabelId: String,
    ): Whitelabel {
        val whitelabels = loadWhitelabels(jwtToken, whitelabelId)
        val whitelabel = whitelabels.find { it.slug == whitelabelId }
            ?: throw NullPointerException("The whitelabel '$whitelabelId' does not exist.")
        setWhitelabel(whitelabel)
        return whitelabel
    }

    /**
     * Return a [Whitelabel] list from an API service.
     */
    private suspend fun loadWhitelabels(
        jwtToken: String,
        whitelabelId: String? = null,
    ): List<Whitelabel> {
        val whitelabelsResp = apiService.getWhitelabels(jwtToken)
        val minVersion: Float = whitelabelsResp.minVersion.toFloat()

        if (Constants.API_MIN_VERSION.toFloat() < minVersion) {
            whitelabelId?.let {
                val whitelabel = whitelabelsResp.whitelabels.find { it.slug == whitelabelId }
                whitelabel?.let { storage.setApiUpdateUrl(whitelabel.url) }
            }
            throw ApiOutdatedException("API is outdated. Current version: ${Constants.API_MIN_VERSION}. Actual version: $minVersion.")
        }

        return whitelabelsResp.whitelabels
    }

    /**
     * Save a [Token] into local storage.
     */
    private fun setToken(token: Token) = storage.setToken(token)

    /**
     * Return a [Token] from local storage.
     */
    fun getToken() = storage.getToken()

    private suspend fun getOrRefreshToken(): Token? {
        val token = storage.getToken()
        if (token != null && token.isExpired()) return refreshToken()
        return token
    }

    /**
     * Return a [Token] from an API service.
     */
    suspend fun loadToken(
        code: String?,
        grantType: String?,
        redirectUri: String?,
        codeVerifier: String?,
    ): Token {
        val whitelabel = getWhitelabel()
            ?: throw NullPointerException("The whitelabel can't be null.")

        val token = apiService.getToken(
            clientId = getClientId(),
            code = code,
            grantType = grantType,
            redirectUri = redirectUri,
            codeVerifier = codeVerifier,
            tokenUrl = whitelabel.tokenUrl
        )
        setToken(token)
        return token
    }

    /**
     * Return a [Token] from an API service.
     */
    suspend fun loadTokenByPassword(
        tokenUrl: String,
        clientId: String,
        username: String?,
        password: String?,
    ): Token {
        val token = apiService.getTokenByPassword(
            tokenUrl = tokenUrl,
            clientId = clientId,
            login = username,
            password = password,
        )
        setToken(token)
        return token
    }

    /**
     * Refresh a [Token].
     * A refreshed token retrieved from API service will be saved locally.
     */
    suspend fun refreshToken(): Token {
        val token = getToken()
        val whitelabel = getWhitelabel()
        if (token == null) throw NullPointerException("The token can't be null.")
        if (whitelabel == null) throw NullPointerException("The whitelabel can't be null.")

        val refreshedToken = apiService.refreshToken(
            tokenUrl = whitelabel.tokenUrl,
            clientId = getClientId(),
            refreshToken = token.refreshToken
        )
        setToken(refreshedToken)
        return refreshedToken
    }

    /**
     * Set client id to local storage.
     * @param clientId
     */
    fun setClientId(clientId: String) = storage.setClientId(clientId)

    /**
     * Get client id from local storage.
     * @return clientId
     */
    fun getClientId(): String = storage.getClientId()

    /**
     * Remove a token from local storage.
     */
    private fun removeToken() = storage.removeToken()

    /**
     * Save a [User] into local storage.
     */
    private fun setUser(user: User) = storage.setUser(user)

    /**
     * Return a [User] from local storage.
     */
    fun getUser(): User? = storage.getUser()

    /**
     * Return a [User] from an API service.
     * @param profileUrl By default, this value will be taken from the [Whitelabel].
     */
    suspend fun loadUser(profileUrl: String? = null): User {
        val token = getOrRefreshToken() ?: throw NullPointerException("The token can't be null.")
        var whitelabel: Whitelabel? = null
        if (profileUrl == null) {
            whitelabel = getWhitelabel()
                ?: throw NullPointerException("The whitelabel can't be null.")
        }

        val user = apiService.getUser(
            profileUrl ?: whitelabel!!.profileUrl,
            "Bearer ${token.accessToken}"
        ).user
        setUser(user)
        return user
    }

    /**
     * Register of a new user
     */
    suspend fun registerUser(
        clientId: String,
        clientSecret: String,
        username: String?,
        password: String?,
        email: String?,
        checkTermsOfUse: Boolean,
    ): RegisterUserResp = apiService.registerUser(
        clientId, clientSecret, username, password, password, email, checkTermsOfUse
    )

    /**
     * Reload a [User] data.
     * A reloaded user data retrieved from API service will be saved locally.

     * @see getUser
     */
    suspend fun reloadUser(): User {
        val token = getOrRefreshToken()
        val whitelabel = getWhitelabel()
        if (token == null || whitelabel == null) throw NullPointerException("The token and/or whitelabel can't be null.")

        val user = apiService.getUser(whitelabel.profileUrl, "Bearer ${token.accessToken}").user
        setUser(user)
        return user
    }

    /**
     * Remove a user from local storage.
     */
    private fun removeUser() = storage.removeUser()

    /**
     * Save a [Whitelabel] into local storage.
     */
    private fun setWhitelabel(whitelabel: Whitelabel) = storage.setWhitelabel(whitelabel)

    /**
     * Return a [Whitelabel] from local storage.
     */
    fun getWhitelabel(): Whitelabel? = storage.getWhitelabel()

    /**
     * Remove a [Whitelabel] from local storage.
     */
    private fun removeWhitelabel() = storage.removeWhitelabel()

    /**
     * Save a [Locale] into local storage.
     */
    fun setLocale(locale: Locale) =
        when {
            Constants.SUPPORTED_LANGUAGES.contains(locale.language) -> storage.setLocale(locale)
            else -> storage.setLocale(Locale.ENGLISH)
        }

    /**
     * Return a [Locale] from local storage.
     */
    fun getLocale() = storage.getLocale()

    /**
     * Just logout. Not used.
     */
    suspend fun logout(logoutUrl: String): String = apiService.logout(logoutUrl)

    /**
     * Clear all local data: [User], [Token], [Whitelabel]
     */
    fun clearLocalData() {
        removeUser()
        removeToken()
        removeWhitelabel()
    }

    /**
     * Proceed payment.
     * @param paymentId
     * @param amount
     * @return [BaseStatusResp]
     */
    suspend fun proceedPayment(paymentId: String?, amount: Int?): BaseStatusResp {
        val token = getOrRefreshToken() ?: throw NullPointerException("The token can't be null.")
        val whitelabel = getWhitelabel() ?: throw NullPointerException("Whitelabel can't be null.")

        return apiService.proceedPayment(
            paymentUrl = whitelabel.url + "/api/v1/payments",
            accessToken = "Bearer ${token.accessToken}",
            paymentId = paymentId,
            amount = amount
        )
    }

    /**
     * Adds data points.
     * @param dataPoints [DataPointModel]
     * @return [BaseStatusResp]
     */
    suspend fun addDataPoints(dataPoints: List<DataPoint>): BaseStatusResp {
        val token = getOrRefreshToken() ?: throw NullPointerException("The token can't be null.")
        val whitelabel = getWhitelabel() ?: throw NullPointerException("Whitelabel can't be null.")

        return apiService.addDataPoints(
            dataPointsUrl = whitelabel.url + "/api/v1/me/data_point_collection",
            accessToken = "Bearer ${token.accessToken}",
            dataPoints = DataPointModel(dataPoints = dataPoints)
        )
    }

    /**
     * Get current quest data
     * @return [QuestData]
     */
    suspend fun getCurrentQuestData(): QuestData {
        val token = getOrRefreshToken() ?: throw NullPointerException("The token can't be null.")
        val whitelabel = getWhitelabel() ?: throw NullPointerException("Whitelabel can't be null.")

        return apiService.getCurrentQuestData(
            url = whitelabel.url + Constants.QUEST_ENDPOINT,
            accessToken = "Bearer ${token.accessToken}"
        )
    }

    /**
     * Get payment information
     * @return [PaymentInfo]
     */
    suspend fun getPaymentInfo(paymentId: String): PaymentInfo {
        val token = getOrRefreshToken() ?: throw NullPointerException("The token can't be null.")
        val whitelabel = getWhitelabel() ?: throw NullPointerException("Whitelabel can't be null.")

        return apiService.getPaymentInfo(
            url = whitelabel.url + String.format(Constants.PAYMENT_INFO_ENDPOINT, paymentId),
            accessToken = "Bearer ${token.accessToken}"
        )
    }

    companion object {
        //private const val TAG = "AuthRepository"

        /**
         * [Repository] singleton instance.
         */
        @Volatile
        private var instance: Repository? = null

        /**
         * Returns [Repository] singleton instance.
         */
        @JvmStatic
        fun getInstance(ctx: Context): Repository {
            return instance ?: synchronized(this) {
                instance ?: Repository(
                    InjectorUtil.provideApiService(ctx),
                    InjectorUtil.provideStorage(ctx)
                ).also {
                    instance = it
                }
            }
        }
    }
}