package com.erogames.auth.sample

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.erogames.auth.ErogamesAuth
import com.erogames.auth.OnResult
import com.erogames.auth.model.DataPoint
import com.erogames.auth.model.PaymentInfo
import com.erogames.auth.model.QuestData
import java.util.*

class MainActivity : AppCompatActivity() {

    private var locale: Locale = Locale.ENGLISH

    private lateinit var loginBtn: Button
    private lateinit var logoutBtn: Button
    private lateinit var signupBtn: Button
    private lateinit var userTextView: TextView
    private lateinit var spinner: Spinner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ErogamesAuth.init(this, "pG8uvkv4nPzsTFWoW_kJnqzdOxa0Us576zastDoROaE")

        loginBtn = findViewById(R.id.login)
        loginBtn.setOnClickListener { ErogamesAuth.login(this, locale) }

        logoutBtn = findViewById(R.id.logout)
        logoutBtn.setOnClickListener { ErogamesAuth.logout(this, true) }

        signupBtn = findViewById(R.id.signup)
        signupBtn.setOnClickListener { ErogamesAuth.signup(this, locale) }
        userTextView = findViewById(R.id.user)

        findViewById<View>(R.id.buy_erogold).setOnClickListener { onBuyErogold() }
        findViewById<View>(R.id.support).setOnClickListener { onSupport() }
        findViewById<View>(R.id.refresh_token).setOnClickListener { onRefreshToken() }
        findViewById<View>(R.id.reload_user).setOnClickListener { onReloadUser() }
        findViewById<View>(R.id.load_whitelabel).setOnClickListener { onLoadWhitelabel() }
        findViewById<View>(R.id.get_whitelabel).setOnClickListener { onGetWhitelabel() }
        findViewById<View>(R.id.proceed_payment).setOnClickListener { onProceedPayment() }
        findViewById<View>(R.id.add_data_points).setOnClickListener { onAddDataPoints() }
        findViewById<View>(R.id.load_current_quest).setOnClickListener { onLoadCurrentQuest() }
        initLangSpinner()
    }

    private fun openUrl(url: String) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()
        updateUI()
    }

    private fun initLangSpinner() {
        spinner = findViewById(R.id.languages)
        ArrayAdapter.createFromResource(
            this,
            R.array.lang_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long,
                ) {
                    if (position > 0) {
                        val lang: String = parent?.getItemAtPosition(position) as String
                        locale = Locale(lang)
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>?) = Unit
            }
        }
    }

    private fun updateUI() {
        Log.d(TAG, "updateUI")
        val user = ErogamesAuth.getUser(this)
        spinner.visibility = if (user == null) View.VISIBLE else View.GONE
        loginBtn.visibility = if (user == null) View.VISIBLE else View.GONE
        signupBtn.visibility = if (user == null) View.VISIBLE else View.GONE
        logoutBtn.visibility = if (user == null) View.INVISIBLE else View.VISIBLE
        userTextView.text = user?.toString() ?: ""

        if (user == null) showAuthDialog()
    }

    private fun showAuthDialog() {
        val alertDialog: AlertDialog = let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setTitle("Authentication")
                setMessage("Please log in/sign up to continue.")
                setPositiveButton("Signup")
                { _, _ -> ErogamesAuth.signup(this@MainActivity, locale) }
                setNegativeButton("Login")
                { _, _ -> ErogamesAuth.login(this@MainActivity, locale) }
            }
                .setCancelable(false)
            builder.create()
        }
        alertDialog.show()
    }

    private fun insufficientBalanceDialog() {
        val alertDialog: AlertDialog = let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setTitle("Warning")
                setMessage("insufficient balance.")
                setPositiveButton("Buy EG")
                { _, _ -> onBuyErogold() }
                setNegativeButton("Cancel")
                { _, _ -> }
            }
            builder.create()
        }
        alertDialog.show()
    }

    private fun onReloadUser() {
        ErogamesAuth.reloadUser(this, object : OnResult<Void?> {
            override fun onSuccess(data: Void?) = updateUI()

            override fun onFailure(t: Throwable?) {
                Log.d(TAG, "onReloadUser: ${t?.message}")
            }
        })
    }

    private fun onBuyErogold() {
        val wl = ErogamesAuth.getWhitelabel(this)
        if (wl != null) {
            val url = when {
                wl.buyErogoldUrl.containsKey(locale.language) -> wl.buyErogoldUrl[locale.language]
                else -> wl.buyErogoldUrl[Locale.ENGLISH.language]
            }
            openUrl(url!!)
        } else {
            Toast.makeText(this, "Whitelabel == null", Toast.LENGTH_SHORT).show()
        }
    }

    private fun onSupport() {
        val wl = ErogamesAuth.getWhitelabel(this)
        if (wl != null) {
            val url = when {
                wl.supportUrl.containsKey(locale.language) -> wl.supportUrl[locale.language]
                else -> wl.supportUrl[Locale.ENGLISH.language]
            }
            openUrl(url!!)
        } else {
            Toast.makeText(this, "Whitelabel == null", Toast.LENGTH_SHORT).show()
        }
    }

    private fun onRefreshToken() {
        ErogamesAuth.refreshToken(this, object : OnResult<Void?> {
            override fun onSuccess(data: Void?) {
                val token = ErogamesAuth.getToken(this@MainActivity)
                Log.d(TAG, "Refresh token: $token")
            }

            override fun onFailure(t: Throwable?) {
                Log.d(TAG, "onRefreshToken: ${t?.message}")
            }
        })
    }

    private fun onLoadWhitelabel() {
        ErogamesAuth.loadWhitelabel(this, object : OnResult<Void?> {
            override fun onSuccess(data: Void?) {
                val whitelabel = ErogamesAuth.getWhitelabel(this@MainActivity)
                Log.d(TAG, "Load whitelabel: $whitelabel")
            }

            override fun onFailure(t: Throwable?) {
                Log.d(TAG, "onLoadWhitelabel: ${t?.message}")
            }
        })
    }

    private fun onGetWhitelabel() {
        val whitelabel = ErogamesAuth.getWhitelabel(this@MainActivity)
        Log.d(TAG, "Get whitelabel: $whitelabel")
    }

    private fun onProceedPayment() {
        val paymentId = UUID.randomUUID().toString()
        val amount = 1
        ErogamesAuth.proceedPayment(this, paymentId, amount, object : OnResult<Void?> {
            override fun onSuccess(data: Void?) {
                Log.d(TAG, "onProceedPayment: success")
                loadPaymentInfo(paymentId)
            }

            override fun onFailure(t: Throwable?) {
                Log.d(TAG, "onProceedPayment: ${t?.message}")
                insufficientBalanceDialog()
            }
        })
    }

    private fun loadPaymentInfo(paymentId: String) {
        ErogamesAuth.loadPaymentInfo(this, paymentId, object : OnResult<PaymentInfo> {
            override fun onSuccess(data: PaymentInfo) {
                Log.d(TAG, "loadPaymentInfo: $data")
            }

            override fun onFailure(t: Throwable?) {
                Log.d(TAG, "loadPaymentInfo: ${t?.message}")
            }
        })
    }

    private fun onAddDataPoints() {
        val dataPoints = listOf(
            DataPoint("here", "be"),
            DataPoint("dragons", "5"),
            DataPoint("current_experience", "12500"),
        )

        ErogamesAuth.addDataPoints(this, dataPoints, object : OnResult<Void?> {
            override fun onSuccess(data: Void?) {
                Log.d(TAG, "onAddDataPoints: success")
            }

            override fun onFailure(t: Throwable?) {
                Log.d(TAG, "onAddDataPoints: ${t?.message}")
            }
        })
    }

    private fun onLoadCurrentQuest() {
        ErogamesAuth.loadCurrentQuest(this, object : OnResult<QuestData> {
            override fun onSuccess(data: QuestData) {
                Log.d(TAG, "onLoadCurrentQuest: $data")
            }

            override fun onFailure(t: Throwable?) {
                Log.e(TAG, "onLoadCurrentQuest: ${t?.message}")
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                ErogamesAuth.REQUEST_CODE_AUTH -> handleOnAuthActivityResult(data)
                ErogamesAuth.REQUEST_CODE_LOGOUT -> Unit
            }
        }
    }

    private fun handleOnAuthActivityResult(data: Intent?) {
        // Get error message in case of unsuccessful authentication
        val authError = data?.getStringExtra(ErogamesAuth.EXTRA_ERROR)
        authError?.let { Toast.makeText(this, it, Toast.LENGTH_LONG).show() }

        // Optionally, get user from intent extra
        // val user: User? = data?.getParcelableExtra(ErogamesAuth.EXTRA_USER)
    }

    companion object {
        private const val TAG = "MainActivity"
    }
}