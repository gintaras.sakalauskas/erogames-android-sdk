# Erogames Analytics

## Integrating into the project

To add this library to the project:

1. Add repository in your project-level `build.gradle`.
```groovy
allprojects {
    repositories {
        ...
        maven {
            url "https://gitlab.com/api/v4/projects/20889762/packages/maven"
        }
    }
}
```
2. Add the following in your app-level `build.gradle`.
```groovy
dependencies {
    ...
    implementation "com.erogames.analytics:analytics:x.y.z"
}
```
The latest version can be taken on the [tags page](https://gitlab.com/sky-network/erogames-android-sdk/-/tags).  
If the tag name is `analytics-v1.2.3` then `<x.y.z>` is `1.2.3`.

3. Initialize the Erogames Analytics before use:
```kotlin
ErogamesAnalytics.init(context, your_client_id)
```

### Log event
To override data used by ErogamesAnalytics internally, add `ea_` prefix to the param name.  
To add custom data, the param name should not be started with `ea_` prefix.

```kotlin
val event = "install"
ErogamesAnalytics.logEvent(context, event)
```
Log event with a payload.
```kotlin
val event = "click"
val params = mapOf(
    "ea_category" to "banners"
)
ErogamesAnalytics.logEvent(this, event, params)
```

### How to debug:
```shell
adb logcat -s EventWorker -s TrackProviderUtil
```
