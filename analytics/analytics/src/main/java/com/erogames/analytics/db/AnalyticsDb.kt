package com.erogames.analytics.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.erogames.analytics.model.TrackInfo

@Database(entities = [TrackInfo::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
internal abstract class AnalyticsDb : RoomDatabase() {

    companion object {
        private const val DB_NAME = "erogames_analytics.db"

        @Volatile
        private var instance: AnalyticsDb? = null

        fun getInstance(context: Context): AnalyticsDb {
            return instance ?: synchronized(this) {
                instance ?: create(context)
                    .also { instance = it }
            }
        }

        private fun create(context: Context): AnalyticsDb =
            Room.databaseBuilder(context, AnalyticsDb::class.java, DB_NAME)
                .allowMainThreadQueries() //TODO
                .build()
    }

    abstract fun trackInfoDao(): TrackInfoDao
}