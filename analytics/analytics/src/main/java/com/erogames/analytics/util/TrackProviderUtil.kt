package com.erogames.analytics.util

import android.content.Context
import android.content.pm.PackageManager
import android.content.pm.ProviderInfo
import android.net.Uri
import android.util.Log
import com.erogames.analytics.BuildConfig
import com.erogames.analytics.R
import com.erogames.analytics.model.TrackInfo
import com.erogames.analytics.repository.EATrackInfoProvider
import java.util.*

internal class TrackProviderUtil private constructor() {

    companion object {
        private const val TAG = "TrackProviderUtil"

        internal fun getOrCreateTrackInfo(ctx: Context): TrackInfo {
            var trackInfo: TrackInfo? = findTrackInfo(ctx)
            if (trackInfo == null) {
                val id = UUID.randomUUID().toString()
                val authority = EATrackInfoProvider.getAuthority(ctx)
                val createdAt = System.currentTimeMillis() / 1000L
                trackInfo = TrackInfo(id, authority, createdAt)
                insertTrackInfo(ctx, trackInfo)
            }

            // In case the TrackInfo came from another app,
            // we copy the found TrackInfo into the current app.
            // If the app from which the TrackInfo was taken will be deleted,
            // the TrackInfo will still keep in the current app.
            if (trackInfo.authority == EATrackInfoProvider.getAuthority(ctx)) {
                Log.d(TAG, "The TrackInfo is taken from the current app.")
            } else {
                Log.d(TAG, "Save TrackInfo into the current app...")
                insertTrackInfo(
                    ctx, trackInfo.copy(
                        authority = EATrackInfoProvider.getAuthority(ctx),
                        createdAt = System.currentTimeMillis() / 1000L
                    )
                )
            }

            return trackInfo
        }

        @JvmStatic
        private fun findTrackInfo(ctx: Context): TrackInfo? {
            Log.i(TAG, "Find TrackInfo...")
            for (provider in getEAProviders(ctx)) {
                val trackInfo = getTrackInfo(ctx, provider.authority)
                if (trackInfo != null) return trackInfo
            }
            Log.i(TAG, "There is no TrackInfo on device.")
            return null
        }

        @JvmStatic
        private fun getEAProviders(ctx: Context): List<ProviderInfo> {
            val providers = mutableListOf<ProviderInfo>()
            var currentProvider: ProviderInfo? = null
            val currentAuthority = EATrackInfoProvider.getAuthority(ctx)

            for (pack in ctx.packageManager.getInstalledPackages(PackageManager.GET_PROVIDERS)) {
                if (pack.providers != null) for (provider in pack.providers) {
                    if (provider.labelRes == R.string.com_erogames_analytics_content_provider_label) {
                        providers.add(provider)
                        if (provider.authority == currentAuthority) {
                            currentProvider = provider
                        }
                    }
                }
            }
            Log.i(TAG, "Found EA providers: ${providers.size}")
            currentProvider?.let {
                providers.remove(it)
                providers.add(0, it)
            }
            return providers
        }

        @JvmStatic
        private fun getTrackInfo(ctx: Context, authority: String?): TrackInfo? {
            Log.i(TAG, "Get TrackInfo from provider: $authority")
            val uri = Uri.parse("content://$authority/${EATrackInfoProvider.TABLE_NAME}")
            val cursor = ctx.contentResolver.query(uri, null, null, null, null)

            if (cursor == null) {
                Log.w(TAG, "Provider does not exists.")
                return null
            }

            if (!cursor.moveToFirst()) {
                Log.i(TAG, "TrackInfo not found.")
                cursor.close()
                return null
            }

            val trackInfo = TrackInfo(
                cursor.getString(cursor.getColumnIndex(TrackInfo.ID)),
                cursor.getString(cursor.getColumnIndex(TrackInfo.AUTHORITY)),
                cursor.getLong(cursor.getColumnIndex(TrackInfo.CREATED_AT))
            )
            Log.i(TAG, "Found TrackInfo: $trackInfo")
            cursor.close()
            return trackInfo
        }

        @JvmStatic
        private fun insertTrackInfo(ctx: Context, trackInfo: TrackInfo): Uri? {
            Log.d(TAG, "Insert TrackInfo: $trackInfo")
            val authority = EATrackInfoProvider.getAuthority(ctx)
            val values = TrackInfo.toContentValues(trackInfo)
            val uri = Uri.parse("content://$authority/${EATrackInfoProvider.TABLE_NAME}")

            return ctx.contentResolver.insert(uri, values)
        }
    }
}