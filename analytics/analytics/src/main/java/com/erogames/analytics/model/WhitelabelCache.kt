package com.erogames.analytics.model

import androidx.annotation.Keep
import kotlinx.serialization.Serializable

@Keep
@Serializable
internal data class WhitelabelCache(
    val whitelabel: Whitelabel,
    val createdAt: Long
)