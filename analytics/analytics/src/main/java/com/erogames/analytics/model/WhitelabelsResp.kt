package com.erogames.analytics.model

import androidx.annotation.Keep
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Keep
@Serializable
internal data class WhiteLabelsResp(
    val whitelabels: List<Whitelabel>,
    @SerialName("min_version")
    val minVersion: String,
)