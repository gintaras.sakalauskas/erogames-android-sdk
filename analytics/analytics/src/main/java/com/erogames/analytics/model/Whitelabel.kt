package com.erogames.analytics.model

import androidx.annotation.Keep
import kotlinx.serialization.Serializable

@Keep
@Serializable
internal data class Whitelabel(
    val slug: String,
    val url: String
)