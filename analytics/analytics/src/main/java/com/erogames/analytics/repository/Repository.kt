package com.erogames.analytics.repository

import android.content.Context
import android.util.Log
import com.erogames.analytics.R
import com.erogames.analytics.api.ApiService
import com.erogames.analytics.model.EventResp
import com.erogames.analytics.model.TrackingDataModel
import com.erogames.analytics.model.Whitelabel
import com.erogames.analytics.model.WhitelabelCache
import com.erogames.analytics.util.InjectorUtil
import com.erogames.analytics.util.Utils

internal class Repository(
    private val whitelabelId: String,
    private val accessKey: String,
    private val apiService: ApiService,
    private val storage: Storage
) {

    suspend fun sendEvent(dataModel: TrackingDataModel): EventResp {
        val whitelabel = loadWhitelabel()

        val wlTrackingUrl = buildWLTrackingUrl(whitelabel)
        return apiService.sendEvent(wlTrackingUrl, dataModel)
    }

    private suspend fun loadWhitelabel(useCache: Boolean = true): Whitelabel {
        if (useCache) {
            val wl = getWhitelabelCache()
            if (wl != null && wl.whitelabel.slug == whitelabelId && Utils.isWlCacheValid(wl)) {
                Log.i(TAG, "The whitelabel is taken from the cache.")
                return wl.whitelabel
            }
        }

        val jwtToken = apiService.getJwtToken(accessKey)
        val whitelabels = apiService.getWhitelabels(jwtToken.token).whitelabels
        val wl = whitelabels.find { it.slug == whitelabelId }
            ?: throw NullPointerException("The whitelabel '$whitelabelId' does not exist.")

        storage.setWhitelabelCache(WhitelabelCache(wl, System.currentTimeMillis() / 1000))
        return wl
    }

    private fun getWhitelabelCache(): WhitelabelCache? = storage.getWhitelabelCache()

    private fun buildWLTrackingUrl(whitelabel: Whitelabel): String {
        if (whitelabel.url.endsWith("/")) {
            return "${whitelabel.url}${ApiService.TRACK_ENDPOINT}"
        }
        return "${whitelabel.url}/${ApiService.TRACK_ENDPOINT}"
    }

    fun setClientId(clientId: String) = storage.setClientId(clientId)

    fun getClientId(): String = storage.getClientId()

    companion object {

        private const val TAG = "Repository"

        @Volatile
        private var instance: Repository? = null

        @JvmStatic
        fun getInstance(ctx: Context): Repository {
            return instance ?: synchronized(this) {
                if (instance == null) {
                    val appMetaData = Utils.getAppMetadata(ctx)
                    val whitelabelId = appMetaData?.getString("erogames_analytics_whitelabel_id")!!
                    val baseAccessKey =
                        ctx.getString(R.string.com_erogames_analytics_access_key)

                    return Repository(
                        whitelabelId,
                        baseAccessKey,
                        InjectorUtil.provideApiService(ctx),
                        InjectorUtil.provideStorage(ctx)
                    ).also { instance = it }
                }
                return instance!!
            }
        }
    }
}