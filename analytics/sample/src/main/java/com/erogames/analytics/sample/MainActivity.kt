package com.erogames.analytics.sample

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.erogames.analytics.ErogamesAnalytics

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //val params1 = mapOf<String, String>()

        val params2 = mapOf(
            ErogamesAnalytics.Param.EA_CATEGORY to "banners"
        )

        val params3 = mapOf(
            ErogamesAnalytics.Param.EA_CATEGORY to "not_banners",
            "custom_param" to "custom_value"
        )

        ErogamesAnalytics.init(this, "pG8uvkv4nPzsTFWoW_kJnqzdOxa0Us576zastDoROaE")

        findViewById<View>(R.id.logEvent1).setOnClickListener {
            ErogamesAnalytics.logEvent(this, "install")
        }

        findViewById<View>(R.id.logEvent2).setOnClickListener {
            ErogamesAnalytics.logEvent(this, "sign_in", params2)
        }

        findViewById<View>(R.id.logEvent3).setOnClickListener {
            ErogamesAnalytics.logEvent(this, "logout", params3)
        }
    }
}